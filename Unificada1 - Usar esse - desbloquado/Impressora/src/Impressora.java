import lejos.nxt.*;
import java.lang.Math;
import lejos.util.Delay;

public class Impressora {
	
	//****************** Atributos Gerais da Classe *********************************//
	
	//variaveis display LCD
	private int display_x;
	private int display_y;
	
	//variaveis da posicao da caneta
	private int globPos_x; //motor cabecote
	private int globPos_y; //motor papel
	
	private Boolean penDown = false;
	
	//valor critico do sensor de luz | menor que esse valor sem papel
	private static int lightSensorCriticalValue = 44;
	
	private LightSensor lightSensor;// = new LightSensor(SensorPort.S1);
	
	private TouchSensor touchSensor;// = new TouchSensor(SensorPort.S2);

	//****************** Funcionalidades Gerais da Classe *********************************//
	
	public Impressora(SensorPort lightSensorPort, SensorPort touchSensorPort) {
		
		display_x = 0;
		display_y = 0;
		globPos_x = 0;
		globPos_y = 0;
		
		lightSensor = new LightSensor(lightSensorPort);
		touchSensor = new TouchSensor(touchSensorPort);
		//faltam os motores??
		
	} 

	public void pullPaper(){
    	cleanDisplay();
    	
    	printOnDisplay("Posicionando Papel");
    	
    	Delay.msDelay(500);
    	
    	Motor.A.setSpeed(300);
    	
    	//Puxa o papel ate que o sensor o reconheca
    	Motor.A.forward();
    	
    	//talvez tenhamos que melhorar essa conta, para ficar mais correto
        while (lightSensor.getLightValue() <= lightSensorCriticalValue){
        	;
        }
        
        Motor.A.stop();
        
        Delay.msDelay(500);
        
        Motor.A.rotate(180);
        
        printOnDisplay("Papel Posicionado");
        
        Delay.msDelay(1000);
    }
    
	public void InitHead () {
    	  cleanDisplay();
    	  
    	  printOnDisplay("Inicia Cabecote");
    	  
    	  //garante que o cabecote nao esta pressionando o sensor
    	  Motor.B.rotate(270);
    	  
    	  Motor.B.setSpeed(450);
    	  
    	  //retorna ate pressionar o sensor
    	  Motor.B.backward();
    	  
    	  while(!touchSensor.isPressed()) {
    		  ;
    	  }
    	  
    	  Motor.B.stop();
    	  
    	  Motor.B.setSpeed(150);
    	  
    	  //avanca o cabecote ate o momento que o sensor nao eh mais pressionado
    	  Motor.B.forward();
    	  
    	  while (touchSensor.isPressed()) {
    		  ;
    	  }
    	  
    	  Motor.B.stop();
    	  
    	  printOnDisplay("Cabe�ote OK");
    	  
    	  Delay.msDelay(1000);
	}
	
	public void cleanDisplay() {
		LCD.clear();
		
		resetDisplayCoordinates();
	}

	public void resetDisplayCoordinates() {
		display_x = 0;
		display_y = 0;
	}
	
	public void printOnDisplay (String str) {
		LCD.drawString(str, display_x, display_y);
		
		display_y++;
	}
	
	//funcao para descobrir quantas graus o motor deve girar para obter uma distancia necessaria
	public int distanceToDegreeMotorHead (float lenghtRequired) {
		//funcao de conversoa obtida com dados expeimentais
		int degree = Math.round(8.17f * lenghtRequired + 1.47f);
		
		return (lenghtRequired == 0? 0 : degree);
	}
	
	//funcao para descobrir quantas graus o motor deve girar para obter uma distancia necessaria	
	public int distanceToDegreeMotorPaper (float lenghtRequired) {
		//funcao de conversoa obtida com dados expeimentais
		int degree = Math.round(8.06f * lenghtRequired + 1.81f);
		
		return (lenghtRequired == 0? 0 : degree);
	}
	
	//posiciona a caneta no ponto posAt_x, posAt_y, podendo desenhar ou nao a linha
	public void MovePenDrawTo(int finalPos_x, int finalPos_y, Boolean draw){
		int currPos_x = globPos_x;
		int currPos_y = globPos_y;

		// posFinal - posInicial = finalPos - currPos 
		int degreesToMove_x = distanceToDegreeMotorHead(finalPos_x - currPos_x); 
		// posFinal - posInicial = finalPos - currPos
		int degreesToMove_y = distanceToDegreeMotorHead(finalPos_y - currPos_y); 
		
		//se for movimento para desenhar a linha, abaixa a caneta
		if (draw)
			DropPen();
			
		//garante velocidades iguais, para o caso de uma diagonal
		Motor.B.setSpeed(300);
		Motor.A.setSpeed(300);
		
		if (degreesToMove_x != 0 && degreesToMove_y != 0 && draw) {
			Motor.B.setSpeed(150);
			Motor.A.setSpeed(150);
		}
		
		Motor.B.rotate(degreesToMove_x, true);//aciona o motor cabecote e nao espera completar o movimento
		Motor.A.rotate(degreesToMove_y, true);//aciona o motor papel junto com o do cabecote e nao espera tb
		
		//enqaunto os motores estiverem se movendo nao faz nada, somente aguarda
		while (Motor.A.isMoving() || Motor.B.isMoving()) {
			;
		}
		
		globPos_x += finalPos_x - currPos_x;
		globPos_y += finalPos_y - currPos_y;
		
		if (draw)
			PullUpPen();
	}
	
	public void DropPen () {
		
		Motor.C.setSpeed(100);
		
		Motor.C.rotate(-21);
		
		penDown = true;
	}
	
	public void PullUpPen () {
		Motor.C.setSpeed(300);
		
		Motor.C.rotate(21);
		
		penDown = false;
	}
	
	public void ejectPaper(){
		cleanDisplay();
		
		printOnDisplay("Finalizando...");
    	
    	Delay.msDelay(500);
    	
    	Motor.A.setSpeed(720);
    	
    	//Puxa o papel ate que o sensor nao o reconheca mais
    	Motor.A.backward();
    	
    	//talvez tenhamos que melhorar essa conta, para ficar mais correto
        while (lightSensor.getLightValue() >= lightSensorCriticalValue - 6){
        	;
        }
        
        Motor.A.stop();
        
        Motor.A.rotate(-540);
        
        printOnDisplay("Papel ejetado");
        
        Delay.msDelay(1000);
	}


}
